#!/bin/bash
# wait.sh

set -e

ADDITION_HOST="$1"
ADDITION_PORT="$2"
SUBSTRACTION_HOST="$3"
SUBSTRACTION_PORT="$4"

shift 4
cmd="$@"

until $(curl --output /dev/null --silent --head --fail http://$ADDITION_HOST:$ADDITION_PORT/addition/add); do
  printf 'Waiting for addition service'
  sleep 5
done

until $(curl --output /dev/null --silent --head --fail http://$SUBSCRIPTION_HOST:$SUBSCRIPTION_PORT/addition/add); do
  printf 'Waiting for subscription service'
  sleep 5
done

exec $cmd