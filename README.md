# L'intégration Continue

<p align="center">
  <b>Liens utiles :</b><br>
  <a href="https://jenkins-ci.org">Jenkins</a> |
  <a href="#">Gitlab-ci</a> |
  <a href="#">DevOps</a> |
  <a href="#">Docker</a> |
  <a href="#">Scrum</a> |
  <a href="#">AWS</a> |
  <a href="#">Google Cloud Plateform</a> |
  <a href="#">Microsoft Azure</a> |
  <img width="200" src="https://img1.etsystatic.com/013/1/6837394/il_570xN.411018633_m4in.jpg">
  <br><br>
</p>


***

## Présentation

Qui suis-je ? 
Expériences. Utilisation de l'intégration continue.
SopraSteria

## C'est quoi l'intégration continue ? Pour quoi faire ?

### Continuus integration
    
* Intégration ( = Push (Git) ou Commit (svn)) fréquente des développements en cours 
* Exécution de tests automatiques sur les sources intégrées
* pyramide des tests
* extension -> TDD
    
### Continuus delivery

* Extension de l'integration continue +
* automatisation de la mise en prod (presse bouton)
            
### Continuus deployment

* Extension du continuus delivery
* Toute modif sur la base de code passe un pipeline de tests
* Mise en prod automatique si les tests sont verts
![alt text](https://wac-cdn.atlassian.com/dam/jcr:84fa9fcf-4ad0-4417-96d3-e5d3387d7f81/CDmicro-600x338-retina2x-B_cicds.png?cdnVersion=ja)   


## Les différents outils

### Jenkins

Outils "historique" open source
```
docker pull jenkins/jenkins:lts
docker run -p 8080:8080 -p 50000:50000 jenkins/jenkins:lts
```
https://github.com/jenkinsci/docker/blob/master/README.md

#### Gitlab-ci

* Gitlab-ci se "branche" sur un dépôt Gitlab 
* à l'arrivée des commits, il clone ou fetch le dépot, et lance un script (que vous devez écrire) via un des "runners" installés en local ou sur d'autres machines.
* selon le code de sortie du script, alors la tâche est considérée comme un succès ou un échec.
* le rapport d’exécution se limite à la sortie console de votre script. pas de prise en charge des sorties xunit etc
* une notification du résultat par email peut être faite.

#### Strider-CD

####  Teamcity

#### Bamboo

# Contexte d'utilisation

## Contexte d'innovation passé

Le PDG de la société "Innovate" a une idée géniale. 
Il va faire une calculatrice qu'il va mettre à disposition sur internet et faire payer ces utilisateurs à l'opération.

* Une étude de marché est réalisée, avec des tests consommateurs qui confirment l'intéret de cette idée.
* Il demande à son responsable opérationnel de réaliser un cahier des charges complet.
* 3 mois plus tard, le cahier des charges est prêt. Il est transmis au directeur informatique
* Le directeur informatique transmets à son équipe IT.
* L'architecte de la cellule IT, crée un dossier de spécifications techniques détaillées
* 1 mois plus tard l'équipe de dev prend en main l'implémentation et réalise le site internet
* 2 mois plus tard le site est développé.
* L'équipe de recette prend la main, trouve des bugs. L'équipe IT corrige
* 1 mois plus tard le site est testé.
* Le projet est alors passé à l'équipe infrastructure, qui réalise des tests de sécurité.
* Elle doit acheter un nouveau serveur. Il faut faire une demande au service des achats qui fait un appel d'offre
* 1 mois plus tard, le serveur est reçu, le site internet est déployé.
* Peu d'utilisateurs sont vraiment intéressés par le site. 
* Pendant ces 8 mois passés, les applications mobiles ont vécu un véritable engouement, 
et les sites internet classiques sont maintenant démodés

## Contexte d'innovation présent/futur

Un employé de la société "Innovate" a une idée géniale, il pense qu'une calculatrice sur internet permettrait de réaliser des bénéfices pour sa société.
Il demande l'accord de sa direction et constitue une équipe pluridisciplinaire:

* un représentant sur marketting et de la direction
* 3 développeurs de la cellule IT
* 1 développeurs de l'infra
* 1 testeur de la cellule de recette
* lui même

Il décide d'utiliser les méthodes et outils suivants:

* devops
développement
tests automatique dans l'outil de CI/CD
déploiement automatique sur la prod
le développeur gère la prod

* scrum

Découpage des besoins en "stories"
Priorisation des stories
Implémentation itérative des stories en fonction de la priorité
Prise en compte des retours utilisateurs pour adapter / ajouter des stories

* déploiement sur un cloud

La même équipe peut travailler en mode devops sans dépendre d'une équipe infra.
Pas de couts de mise en place de l'infra
Pas de couts de suppression de l'infra si le site ne fonctionne pas

La stratégie est la suivante :

```
Rendre disponible le plus tôt possible, une version limitée de la calculatrice aux utilisateurs pour recueillir 
les retours terrains et adapter si besoin le produit. Principe du "fail fast".
```

# Travaux pratiques

```
Le début du corrigé est dans ce projet.
A vous d'apprendre en vous amusant et par la pratique les concepts.
```

# Des outils
https://www.google.fr/url?q=https://m.youtube.com/watch%3Fv%3DNUWCrREXHmU&sa=U&ved=0ahUKEwiAxIWPt4TYAhXJEVAKHQUmB3kQtwIIDjAB&usg=AOvVaw2D_Cxthm6nCfFevS9BPCDg
